---
layout: page
title: About
permalink: /about/
---
Hi. I'm VideoToaster.

I'm a young software and web developer.

I'm fluent in:
* C#, C, C++
* Python
* Visual Basic (regrettably)
* Javascript
* PHP (thankfully)

I know the following markup langs:
* HTML5/XHTML1.1
* CSS
* Markdown
* Gophermaps

I have alternate sites at these places:
* [videotoaster.ga](https://videotoaster.ga)
* [video.catvibers.me](https://video.catvibers.me)
* [videotoaster.neocities.org](https://videotoaster.neocities.org)
* [Twitter](https://twitter.com/videotoasterart) (mostly [mastodon](https://mastodon.lol/videotoaster) though)
